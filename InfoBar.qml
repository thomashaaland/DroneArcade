import QtQuick 2.15
import QtQuick.Controls
import QtQuick.Layouts


Item {
    function recieveCoords(x, y) {
        coordDisplay.text = "(" + parseInt(x) + ", " + parseInt(y) + ")";
        progressbarForBattery.value = progressbarForBattery.value - 0.0001
    }

    // Used to change the color of the progressbar
    function colorOfProgressBar()
    {
        var handValue = progressbarForBattery.value
        var handColor;
        if(handValue >= 0.5)
        {
            handColor = "green"
        }
        else if(handValue < 0.5 && handValue >= 0.3)
        {
            handColor = "orange"
        }
        else
        {
            handColor = "red"
        }
        return handColor
    }



    anchors.fill: parent
    RowLayout
    {
        spacing: 12
        anchors.fill: parent
        Item { Layout.fillWidth: true }
        ColumnLayout
        {
            Label {
                text: "Coordinates "
                font.pointSize: 12
                font.bold: true
            }
            Label {
                id: coordDisplay
                text: "(0,0)"
                font.pointSize: 10
            }
        }
        Item {
            Layout.fillWidth: true
        }
        ColumnLayout
        {
            Label {
                text: "Weather "
                font.pointSize: 12
                font.bold: true
            }
            Label {
                id: weather
                text: "25\xB0 "
                font.pointSize: 10
                Image {
                    id: sun
                    y: parent.height/11
                    x: parent.width
                    source: "qrc:/icons/32x32/sun.png"

                }
            }


        }
        Item {
            Layout.fillWidth: true
        }
        ColumnLayout
        {


            ProgressBar {
                id: progressbarForBattery
                value: 1

                background: Rectangle {
                    implicitWidth: 200
                    implicitHeight: 6
                    color: "#e6e6e6"
                    radius: 3
                }
                contentItem: Item {
                    implicitWidth: 200
                    implicitHeight: 4

                    Rectangle {
                        width: progressbarForBattery.visualPosition * parent.width
                        height: parent.height
                        radius: 2
                        color: colorOfProgressBar()
                    }
                }
            }
        }
        Item { Layout.fillWidth: true }
    }
}

