import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Layouts
import QtQuick.Controls

Window {
    id: window
    width: 860
    height: 480
    visible: true
    title: qsTr("Hello World")
    Rectangle {
        anchors.fill: parent
        MapArea { onSend: function(xCord, yCord) { drone.recieve(xCord, yCord) } }
        Drone{ id:drone; onSendCoords: function(x, y) { info.recieveCoords(x, y) } }
    }
    ColumnLayout {
        anchors.fill: parent
        Rectangle {
            Layout.alignment: Qt.AlignTop
            Layout.fillWidth: parent.width
            Layout.maximumHeight: 50
            Layout.minimumHeight: 50
            color: Qt.rgba(0/255., 0/255., 0/255., 0.6)
            InfoBar{ id: info }
        }
        Rectangle {
            Layout.alignment: Qt.AlignBottom
            Layout.fillWidth: parent.width
            Layout.maximumHeight: 200
            Layout.minimumHeight: 120
            color: Qt.rgba(0/255., 0/255., 0/255., 0.6)
            ControlPanel {
                onSend: function(xCord, yCord) { drone.recieve(xCord, yCord) }
                onSendTransportMode: function(mode) { drone.recieveMode(mode) }
                onSendSpeedMode: function(speedMode) { drone.recieveSpeedMode(speedMode) }
                onSendStartStop: function() { drone.recieveStartStop() }
            }
        }
    }
}
