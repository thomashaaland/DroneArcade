# ArcadeDrone
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
This is a touchscreen application that simulates the controlling of a selfdriving drone that can carry people around. 


![alt picture](DroneApp.png)
## Requirments
* [QT](https://www.qt.io/)

Required :
```
QT 6.3.2
```
Install the game:
```
Build and Run with QT
```

## Usage

```
Click on the map to make the drone fly or drive to that location. The HUD will tell how much battery you have, coordinates, latitude, and drive mode. You can switch between driving and flying with the dropdown menu

```



### Contributing
*Thomas Haaland
*Paal Marius Haugen
*Joel Fredin


### Thomas Haaland
Controlpanel and rotation/speed math

### Paal Marius Bruun Haugen
Drone moving and background layout

### Joel Fredin
Statusbar

## License

