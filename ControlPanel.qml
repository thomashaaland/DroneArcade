import QtQuick
import QtQuick.Layouts
import QtQuick.Controls
import QtQuick.Window


Item {
    anchors.fill: parent
    signal send(int xCord, int yCord)
    signal sendTransportMode(int mode)
    signal sendSpeedMode(int speed)
    signal sendStartStop()
    RowLayout {
        spacing: 12
        anchors.fill: parent
        Item {
            Layout.fillWidth: true
        }
        Button {
            icon.name: "menuExpand"
            onReleased: function()
            { if (icon.name === "menuExpand")
                {
                    icon.name = "menuCollapse"
                }
                else {
                    icon.name = "menuExpand"
                }
                sendStartStop()
            }
        }
        Item {
            Layout.fillWidth: true
        }
        TextField { id: xCord; placeholderText: "x:" }
        TextField { id: yCord; placeholderText: "y:" }
        Button {
            text: "Go"
            onClicked: function() {send(parseInt(xCord.text), parseInt(yCord.text));
                xCord.text = ""; yCord.text = ""
            }
        }

        Item {
            Layout.fillWidth: true
        }
        ComboBox {
            id: transportMode
            currentIndex: 1
            model: ["Drive", "Fly"]
            onCurrentIndexChanged: function() { sendTransportMode(transportMode.currentIndex) }
        }
        Item {
            Layout.fillWidth: true
        }
        Item  {
            Layout.fillWidth: childrenRect
            Layout.alignment: Qt.AlignTop
            Dial {
                id: speedDial
                value: 0.5
                from: 0.25
                to: 0.75
                snapMode: "SnapAlways"
                stepSize: 0.25
                onValueChanged: function() {
                    if (value === 0.25)
                    {
                        speedIndicator.text = "Slow"
                    }
                    else if (value === 0.5)
                    {
                        speedIndicator.text = "Medium"
                    }
                    else
                    {
                        speedIndicator.text = "Fast"
                    }
                    sendSpeedMode(value)
                }
            }
            Label {
                id: speedIndicator
                text: "Medium"
            }
        }
        Item {
            Layout.fillWidth: true
        }
    }
}
