import QtQuick 2.15
import QtQuick.Layouts
import QtQuick.Controls
import QtQuick.Window

Image {
    scale: Qt.KeepAspectRatio
    anchors.fill: parent
    source: "qrc:/images/background.png"
    id: map
    signal send(int xCord, int yCord)
    MouseArea {
        anchors.fill: parent
        property int xCord: mouseX
        property int yCord: mouseY
        onClicked: {
            parent.send(xCord, yCord)
        }
    }
}

