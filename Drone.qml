import QtQuick
import QtMultimedia
import QtQuick.Controls

Rectangle{
    id: recct
    width: 2
    height: 2
    color: "transparent"
    property real jerk: 100
    property int xDroneCord: 100
    property int yDroneCord: 100
    property int travelDistance: 0
    property int orientation: 0
    property int speedMode: 1
    property bool turnedOn: true
    property SoundEffect sound: flySound
    x: 100
    y: 100

    signal sendCoords(int xCord, int yCord)
    onXChanged: sendCoords(x, y)
    onYChanged: sendCoords(x, y)

    Image {
        id: vesselIcon
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        source: "qrc:/icons/32x32/droneIconColors.png"
        rotation: 180
    }

    function dotProd(x, y, xp, yp) {
        return x*xp + y*yp
    }

    function calcDist(x, y, xp, yp) {
        var dx = xp - x
        var dy = yp - y
        return Math.sqrt(dotProd(dx, dy, dx, dy))
    }

    function getAngle(x, y, xp, yp) {
        var dx = xp - x
        var dy = yp - y
        return -Math.atan2(dx, dy) * 180 / Math.PI
    }

    function recieve(xCord, yCord) {
        if (turnedOn) {
            travelDistance = calcDist(xDroneCord, yDroneCord, xCord, yCord)
            orientation = getAngle(xDroneCord, yDroneCord, xCord, yCord)
            xDroneCord = xCord
            yDroneCord = yCord
            animation.start()
            sound.play()
        }
    }

    function recieveMode(mode) {
        if (mode === 0) {
            vesselIcon.source = "qrc:/icons/32x32/droneCarIconColors.png"
            jerk = 5*(speedMode+1)*(speedMode+1)*(speedMode+1)
            sound = driveSound
        } else if (mode === 1) {
            vesselIcon.source = "qrc:/icons/32x32/droneIconColors.png"
            jerk = 100*(speedMode+1)*(speedMode+1)*(speedMode+1)
            sound = flySound
        }
    }

    function recieveSpeedMode(mode) {
        speedMode = mode
    }

    function recieveStartStop() {
        turnedOn = !turnedOn
    }


    SoundEffect {
        id: flySound
        source: "qrc:/sound/travling.wav"
    }
    SoundEffect {
        id: driveSound
        source: "qrc:/sound/driving.wav"
    }


    SequentialAnimation {
        id: animation
        running: false
        NumberAnimation {
            id: rotAnimation
            target: recct
            property: "rotation"
            to: orientation
            duration: Math.abs(orientation - rotation)
            easing.type: Easing.InOutCirc
        }
        ParallelAnimation {
            id: posAnimation
            NumberAnimation {
                id: xAnimation
                target: recct
                property: "x"
                to: xDroneCord
                duration: 1000*Math.cbrt(6*travelDistance/jerk)
                easing.type: Easing.InOutCubic
            }
            NumberAnimation {
                id: yAnimation
                target: recct
                property: "y"
                to: yDroneCord
                duration: 1000*Math.cbrt(6*travelDistance/jerk)
                easing.type: Easing.InOutCubic
            }
        }
    }
}
